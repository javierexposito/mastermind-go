package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/javierexposito/mastermind-go/pkg/mastermind"
	"gitlab.com/javierexposito/mastermind-go/pkg/mastermind/storage"
)

func TestHome(t *testing.T) {
	assert := require.New(t)

	s := storage.InMemoryStorage{
		Games: map[string]mastermind.Game{},
	}

	m := Mastermind{
		Storage: s,
	}

	req, err := http.NewRequest("GET", "/", nil)

	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(m.Home)
	handler.ServeHTTP(rr, req)

	assert.Equal(http.StatusNoContent, rr.Code)
}

func TestNewGame(t *testing.T) {
	assert := require.New(t)

	s := storage.InMemoryStorage{
		Games: map[string]mastermind.Game{},
	}

	m := Mastermind{
		Storage: s,
	}
	gameId := "abcde"

	req, err := http.NewRequest("POST", fmt.Sprintf("/new?gameId=%s", gameId), nil)

	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(m.StartGame)
	handler.ServeHTTP(rr, req)

	assert.Equal(http.StatusNoContent, rr.Code)
}

func TestMastermind_FeedbackFailsWhenGameDoesNotExist(t *testing.T) {
	assert := require.New(t)

	s := storage.InMemoryStorage{
		Games: map[string]mastermind.Game{},
	}

	m := Mastermind{
		Storage: s,
	}

	c := colorRequest{
		C1: "2", C2: "2", C3: "1", C4: "1",
	}

	jsonGuess, err := json.Marshal(c)

	if err != nil {
		assert.Error(err)
	}

	// Ask for feedback
	gameId := "invalid-id"
	req, err := http.NewRequest("POST", fmt.Sprintf("/play?gameId=%s", gameId), bytes.NewBuffer(jsonGuess))

	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(m.Feedback)
	handler.ServeHTTP(rr, req)

	assert.Equal(http.StatusInternalServerError, rr.Code)

	body, err := ioutil.ReadAll(rr.Body)
	assert.NoError(err)

	var response map[string]interface{}

	err = json.Unmarshal(body, &response)

	assert.NoError(err)

	assert.Contains(response, "error")
	assert.Equal(fmt.Sprintf("no game with id %s was created", gameId), response["error"])

}

func TestFeedback(t *testing.T) {
	assert := require.New(t)

	s := storage.InMemoryStorage{
		Games: map[string]mastermind.Game{},
	}

	m := Mastermind{
		Storage: s,
	}

	//Create game
	gameId := "abcde"

	req, err := http.NewRequest("POST", fmt.Sprintf("/new?gameId=%s", gameId), nil)

	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(m.StartGame)

	handler.ServeHTTP(rr, req)

	assert.Equal(http.StatusNoContent, rr.Code)

	c := colorRequest{
		C1: "2", C2: "2", C3: "1", C4: "1",
	}
	jsonGuess, err := json.Marshal(c)

	if err != nil {
		assert.Error(err)
	}

	// Ask for feedback
	req, err = http.NewRequest("POST", fmt.Sprintf("/play?gameId=%s", gameId), bytes.NewBuffer(jsonGuess))

	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()
	handler = http.HandlerFunc(m.Feedback)
	handler.ServeHTTP(rr, req)

	body, err := ioutil.ReadAll(rr.Body)
	assert.NoError(err)

	var response map[string]interface{}
	err = json.Unmarshal(body, &response)
	assert.NoError(err)

	assert.Contains(response, "colorOkPositionOK")
	assert.Contains(response, "colorOkPositionKO")
}

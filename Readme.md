Mastermind
==========

This is a simple go project to play Mastermind game https://en.wikipedia.org/wiki/Mastermind_(board_game)
### Testsuite

```
$ make test
```
### Linters

Run the gofmt, goimports and golangci-lint linters

```
$ make lint
```

### Compiling the project: linux / osx
```
$ make build-linux
$ make build-osx
```

### Run the project

Run the go project with (requires go 1.10.1):
```
$ make up
```
... Or simply compile the project and execute the binary:
```
$ make build-linux
$ ./mastermind
```

### HOW TO PLAY?

Mastermind is played through an API.

Create new game using http://localhost:8080/start?gameId=XXXX
Using gameId as identifier to play your game.

```
$ curl -X GET 'http://localhost:8000/start?gameId=mygame-id'
```

Ask for feedback using http://localhost:8080/play?gameId=XXXX
The guess should be pass in the body of the request with the format {"C1" : "1", "C2" : "1", "C3" : "1", "C4" : "1"}, being C1, C2, C3, C4 the four colors of the guess.
Colors are represented by numbers between 0 and 5.

```
$ curl -X POST 'http://localhost:8080/play?gameId=mygame-id'  --data '{"C1" : "1", "C2" : "1", "C3" : "1", "C4" : "1"}'
```

This request should return a response in json format with the following information:
```
{
    "colorOkPositionOK": X
    "colorOkPositionKO": Y
}
```
With (X >= 0 and X <= 4) and (Y >= 0 and Y <= 4) and (X+Y >= 0 and X+Y <= 4)



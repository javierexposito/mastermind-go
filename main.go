package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"

	"log"

	"github.com/gorilla/mux"
	"gitlab.com/javierexposito/mastermind-go/pkg/mastermind"
	"gitlab.com/javierexposito/mastermind-go/pkg/mastermind/storage"
)

type Mastermind struct {
	Storage storage.Storage
}

func main() {
	s := storage.InMemoryStorage{
		Games: map[string]mastermind.Game{},
	}

	m := Mastermind{
		Storage: s,
	}

	router := mux.NewRouter()
	router.HandleFunc("/", m.Home).Methods("GET")
	router.HandleFunc("/start", m.StartGame).Methods("GET")
	router.HandleFunc("/play", m.Feedback).Methods("POST")
	log.Fatal(http.ListenAndServe(":8080", router))
}

func (m *Mastermind) Home(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusNoContent)
}

func (m *Mastermind) StartGame(w http.ResponseWriter, r *http.Request) {
	gameId := r.FormValue("gameId")

	if gameId == "" {
		payload := map[string]string{"error": "invalid gameId"}
		createResponse(w, http.StatusInternalServerError, payload)

		return
	}

	game := mastermind.Create()

	err := m.Storage.Store(gameId, game)

	if err != nil {
		payload := map[string]string{"error": err.Error()}
		createResponse(w, http.StatusInternalServerError, payload)

		return
	}

	w.WriteHeader(http.StatusNoContent)
}

type colorRequest struct {
	C1, C2, C3, C4 string
}

func (m *Mastermind) Feedback(w http.ResponseWriter, r *http.Request) {

	// Read body request with colors sequence
	defer r.Body.Close()

	var c colorRequest

	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		payload := map[string]string{"error": err.Error()}
		createResponse(w, http.StatusInternalServerError, payload)

		return
	}

	err = json.Unmarshal(body, &c)

	if err != nil {
		payload := map[string]string{"error": err.Error()}
		createResponse(w, http.StatusInternalServerError, payload)

		return
	}

	c1, _ := strconv.Atoi(c.C1)
	c2, _ := strconv.Atoi(c.C2)
	c3, _ := strconv.Atoi(c.C3)
	c4, _ := strconv.Atoi(c.C4)

	ask := mastermind.Game{
		Colors: map[int]int{
			0: c1,
			1: c2,
			2: c3,
			3: c4,
		},
	}

	gameId := r.FormValue("gameId")
	current, err := m.Storage.Get(gameId)

	if err != nil {
		payload := map[string]string{"error": err.Error()}
		createResponse(w, http.StatusInternalServerError, payload)

		return
	}

	if err != nil {
		payload := map[string]string{"error": err.Error()}
		createResponse(w, http.StatusInternalServerError, payload)

		return
	}

	f := mastermind.Check(current, ask)

	createResponse(w, http.StatusOK, f)
}

func createResponse(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	_, _ = w.Write(response)
}

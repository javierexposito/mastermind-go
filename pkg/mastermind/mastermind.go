package mastermind

import "math/rand"

const COLORS = 6

type Game struct {
	Colors map[int]int
}

func Create() Game {

	colors := map[int]int{}

	for i := 0; i < 4; i++ {
		colors[i] = rand.Intn(COLORS - 1)
	}

	return Game{
		Colors: colors,
	}
}

func Check(current, ask Game) map[string]int {

	feedback := make(map[string]int)

	feedback["colorOkPositionOK"] = 0
	feedback["colorOkPositionKO"] = 0

	var alreadyCheckedColors []int

	for i := 0; i < 4; i++ {
		if current.Colors[i] == ask.Colors[i] {
			feedback["colorOkPositionOK"]++
		} else {
			for j := 0; j < 4; j++ {
				if current.Colors[j] == ask.Colors[i] && !contains(alreadyCheckedColors, ask.Colors[i]) {
					feedback["colorOkPositionOK"]++
					break
				}
			}
			alreadyCheckedColors = append(alreadyCheckedColors, ask.Colors[i])
		}
	}

	return feedback
}

func contains(a []int, x int) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}

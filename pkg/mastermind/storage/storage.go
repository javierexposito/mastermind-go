package storage

import (
	"fmt"

	"gitlab.com/javierexposito/mastermind-go/pkg/mastermind"
)

type Storage interface {
	Store(id string, g mastermind.Game) error
	Get(id string) (mastermind.Game, error)
}

type InMemoryStorage struct {
	Games map[string]mastermind.Game
}

func (s InMemoryStorage) Get(id string) (mastermind.Game, error) {
	if game, ok := s.Games[id]; !ok {
		return mastermind.Game{}, fmt.Errorf("no game with id %s was created", id)
	} else {
		return game, nil
	}
}

func (s InMemoryStorage) Store(id string, g mastermind.Game) error {
	s.Games[id] = g

	return nil
}

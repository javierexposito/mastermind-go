package mastermind_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/javierexposito/mastermind-go/pkg/mastermind"
)

const RED = 0
const BLUE = 1
const GREEN = 2
const YELLOW = 3

func TestCodemaker_Create(t *testing.T) {
	g := mastermind.Create()

	assert.IsType(t, mastermind.Game{}, g)
}

func TestCodemaker_Check(t *testing.T) {

	for _, testData := range []struct {
		colors1          map[int]int
		colors2          map[int]int
		expectedFeedback map[string]int
	}{
		{
			colors1: map[int]int{
				0: RED,
				1: RED,
				2: RED,
				3: RED,
			},
			colors2: map[int]int{
				0: GREEN,
				1: GREEN,
				2: GREEN,
				3: GREEN,
			},
			expectedFeedback: map[string]int{
				"colorOkPositionOK": 0,
				"colorOkPositionKO": 0,
			},
		},
		{
			colors1: map[int]int{
				0: RED,
				1: RED,
				2: RED,
				3: RED,
			},
			colors2: map[int]int{
				0: RED,
				1: GREEN,
				2: GREEN,
				3: GREEN,
			},
			expectedFeedback: map[string]int{
				"colorOkPositionOK": 1,
				"colorOkPositionKO": 0,
			},
		},
		{
			colors1: map[int]int{
				0: RED,
				1: YELLOW,
				2: BLUE,
				3: BLUE,
			},
			colors2: map[int]int{
				0: RED,
				1: BLUE,
				2: YELLOW,
				3: GREEN,
			},
			expectedFeedback: map[string]int{
				"colorOkPositionOK": 1,
				"colorOkPositionKO": 2,
			},
		},
		{
			colors1: map[int]int{
				0: GREEN,
				1: BLUE,
				2: RED,
				3: GREEN,
			},
			colors2: map[int]int{
				0: YELLOW,
				1: YELLOW,
				2: BLUE,
				3: BLUE,
			},
			expectedFeedback: map[string]int{
				"colorOkPositionOK": 0,
				"colorOkPositionKO": 1,
			},
		},
		{
			colors1: map[int]int{
				0: GREEN,
				1: BLUE,
				2: RED,
				3: YELLOW,
			},
			colors2: map[int]int{
				0: GREEN,
				1: BLUE,
				2: RED,
				3: YELLOW,
			},
			expectedFeedback: map[string]int{
				"colorOkPositionOK": 4,
				"colorOkPositionKO": 0,
			},
		},
	} {
		game1 := mastermind.Game{Colors: testData.colors1}
		game2 := mastermind.Game{Colors: testData.colors2}

		assert.Equal(t, testData.expectedFeedback, mastermind.Check(game1, game2))

	}
}
